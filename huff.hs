import Data.List
import Data.Ord (comparing)
 
data HTree a = Leaf a | Branch (HTree a) (HTree a) deriving Show

 -- Example : "ana are mere" <=> huffman [('a', 3), ('n', 1), ('r', 2),
 -- ('e', 3), (' ', 2), ('m', 1)]
 
huffman :: (Ord a, Ord fr, Num fr) => [(a, fr)] -> [(a, [Char] )]
huffman freq = sortBy (comparing fst) $ serialize $
	htree $ sortBy (comparing snd) $ [(Leaf x, fr) | (x, fr) <- freq]
  where 
        htree [(t, _)] = t
        htree ((t1, fr1) : (t2, fr2) : queueTail) =
                htree $ insertBy (comparing snd) (Branch t1 t2, fr1 + fr2) queueTail
        serialize (Branch l r) =
                [(x, '0' : code) | (x, code) <- serialize l] ++
                [(x, '1' : code) | (x, code) <- serialize r]
        serialize (Leaf x) = [(x, "")]
